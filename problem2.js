// Function to retrieve information about the last car in the inventory
function getLastCarInfo(inventory) {
  // Check if the inventory is not empty
  if (inventory.length > 0) {
    // Get the index of the last car in the inventory
    let lastCarIndex = inventory.length - 1;

    // Return information about the last car
    return inventory[lastCarIndex];
  } else {
    // Return null if the inventory is empty
    return null;
  }
}

// Export the function for use in other files
module.exports = getLastCarInfo;
