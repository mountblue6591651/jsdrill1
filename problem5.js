// Import the function to get all car years from problem4.js
let getAllCarYears = require("./problem4.js");

// Function to filter and return car years for cars older than 2000
function getOldCars(inventory) {
  // Check if the inventory is empty
  if (inventory.length < 1) {
    console.log("Inventory is empty");
    return null;
  } else {
    // Get an array containing all car years from the inventory
    let allCarYears = getAllCarYears(inventory);

    // Initialize an array to store car years for cars older than 2000
    let oldCars = [];

    // Iterate through all car years
    for (let index = 0; index < allCarYears.length; index++) {
      // Check if the car year is greater than 2000
      if (allCarYears[index] > 2000) {
        // If true, add the car year to the oldCars array
        oldCars.push(allCarYears[index]);
      }
    }

    // Return the array containing car years for cars older than 2000
    return oldCars;
  }
}

// Export the function for use in other files
module.exports = getOldCars;
