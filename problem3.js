// Function to sort car models alphabetically from the inventory
function sortCarsAlphabetically(inventory) {
  // Check if the inventory is empty
  if (inventory.length < 1) {
    console.log("Inventory is empty");
    return null;
  } else {
    // Initialize an array to store all car models
    let carModels = [];

    // Iterate through the inventory to collect car models
    for (let index = 0; index < inventory.length; index++) {
      // Add the car model to the carModels array
      carModels.push(inventory[index].car_model);
    }

    // Sort the carModels array alphabetically
    carModels.sort();

    // Return the array containing car models sorted alphabetically
    return carModels;
  }
}

// Export the function for use in other files
module.exports = sortCarsAlphabetically;
