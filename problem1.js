// Function to retrieve car information by ID from the inventory
function getCarInfoById(inventory, id) {
  // Get the total number of cars in the inventory
  let totalCars = inventory.length;

  // Check if the inventory is empty
  if (!totalCars) {
    console.log("Inventory is empty");
    return -1; // Return -1 to indicate an empty inventory
  } else if (id < 1 || id > totalCars - 1) {
    // Check if the entered ID is outside the valid range
    console.log("Invalid ID entered");
    return -1; // Return -1 to indicate an invalid ID
  }

  // Iterate through the inventory to find the car with the specified ID
  for (let index = 0; index < totalCars; index++) {
    if (inventory[index].id === id) {
      // Return the car information if the ID is found
      return inventory[index];
    }
  }

  // Return null if the car with the specified ID is not found
  return null;
}

// Export the function for use in other files
module.exports = getCarInfoById;
