// Importing the inventory data and the sortCarsAlphabetically function
const inventory = require("../data.js");
let sortCarsAlphabetically = require("../problem3.js");

// Retrieve an array containing car models sorted alphabetically using the sortCarsAlphabetically function
let sortedCars = sortCarsAlphabetically(inventory);

// Check if the sortedCars array is not empty
if (sortedCars) {
  // Log the sortedCars array
  console.log(sortedCars);
}
