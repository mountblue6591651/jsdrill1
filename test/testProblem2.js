// Importing the inventory data and the getLastCarInfo function
const inventory = require("../data");
let getLastCarInfo = require("../problem2");

// Retrieve information about the last car in the inventory using the getLastCarInfo function
let lastCarInfo = getLastCarInfo(inventory);

// Check if information about the last car is available
if (lastCarInfo) {
  // Log a formatted string with the make and model of the last car
  console.log(`Last car is a ${lastCarInfo.car_make} ${lastCarInfo.car_model}`);
} else {
  // Log a message indicating that the inventory is empty
  console.log(`Inventory is empty`);
}
