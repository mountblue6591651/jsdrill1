// Importing the inventory data and the getCarInfoById function
const inventory = require("../data");
let getCarInfoById = require("../problem1");

// Retrieve information about the car with ID 33 using the getCarInfoById function
let car = getCarInfoById(inventory, 33);

// Check if a valid car object is returned (-1 indicates an error or not found)
if (car !== -1) {
  // Check if a car object is returned
  if (car) {
    // Log the car information in a formatted string
    console.log(
      `Car ${car.id} is a ${car.car_year} ${car.car_make} ${car.car_model}`
    );
  } else {
    // Log a message indicating that the car with ID 33 is not found
    console.log(`Car 33 not found in the inventory.`);
  }
}
