// Importing the inventory data and the getOldCars function
const inventory = require("../data.js");
let getOldCars = require("../problem5.js");

// Retrieve an array containing car years for cars older than 2000 using the getOldCars function
let oldCars = getOldCars(inventory);

// Check if the oldCars array is not empty
if (oldCars) {
  // Log the length of the oldCars array
  console.log(oldCars.length);
}
