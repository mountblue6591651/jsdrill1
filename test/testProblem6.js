// Importing the inventory data and the getBMWAndAudi function
const inventory = require("../data.js");
let getBMWAndAudi = require("../problem6.js");

// Retrieve an array containing BMW and Audi cars using the getBMWAndAudi function
let BMWAndAudi = getBMWAndAudi(inventory);

// Check if the BMWAndAudi array is not empty
if (BMWAndAudi) {
  // Log the JSON-stringified representation of the BMWAndAudi array with indentation for readability
  console.log(JSON.stringify(BMWAndAudi, null, 2));
}
