// Importing the inventory data and the getAllCarYears function
const inventory = require("../data.js");
let getAllCarYears = require("../problem4.js");

// Retrieve an array containing all car years from the inventory using the getAllCarYears function
let allCarYears = getAllCarYears(inventory);

// Check if the allCarYears array is not empty
if (allCarYears) {
  // Log the allCarYears array
  console.log(allCarYears);
}
