// Function to filter BMW and Audi cars from the inventory
function getBMWAndAudi(inventory) {
  // Check if the inventory is empty
  if (inventory.length < 1) {
    console.log("Inventory is empty");
    return null;
  } else {
    // Initialize an array to store BMW and Audi cars
    let BMWAndAudi = [];

    // Iterate through the inventory
    for (let index = 0; index < inventory.length; index++) {
      // Check if the car make is "Audi" or "BMW"
      if (
        inventory[index].car_make === "Audi" ||
        inventory[index].car_make === "BMW"
      ) {
        // If true, add the car to the BMWAndAudi array
        BMWAndAudi.push(inventory[index]);
      }
    }

    // Check if there are no BMW and Audi cars in the array
    if (BMWAndAudi.length === 0) {
      console.log("No BMW and Audi cars available");
      return null;
    } else {
      // Return the array containing BMW and Audi cars
      return BMWAndAudi;
    }
  }
}

// Export the function for use in other files
module.exports = getBMWAndAudi;
