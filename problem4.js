// Function to retrieve an array containing all car years from the inventory
function getAllCarYears(inventory) {
  // Check if the inventory is empty
  if (inventory.length < 1) {
    console.log("Inventory is empty");
    return null;
  } else {
    // Initialize an array to store all car years
    let allCarYears = [];

    // Iterate through the inventory to collect car years
    for (let index = 0; index < inventory.length; index++) {
      // Add the car year to the allCarYears array
      allCarYears.push(inventory[index].car_year);
    }

    // Return the array containing all car years
    return allCarYears;
  }
}

// Export the function for use in other files
module.exports = getAllCarYears;
